import { Component, EventEmitter, OnInit, Output } from '@angular/core';


export let universalSettings:any = 'joystick'

@Component({
  selector: 'app-settings-screen',
  templateUrl: './settings-screen.component.html',
  styleUrls: ['./settings-screen.component.css']
})
export class SettingsScreenComponent implements OnInit {

  setting:string = 'joystick'
  buttonSettings:string = 'setting'
  @Output() changeSetting: EventEmitter<string> = new EventEmitter<string>()
  constructor() { }

  ngOnInit(): void {
  }

  clickButton() { 
    console.log('premuto', this.setting)
    if (this.setting === 'joystick') {
      this.setting = 'gamepad'
      console.log('bro')
    } else { 
      this.setting = 'joystick'
      console.log('bro2')


    }
    this.changeSetting.emit(this.setting)
  }

  clickButtonSettings() { 
    if (this.buttonSettings == 'setting') {
      this.buttonSettings = 'close'
    } else { 
      this.buttonSettings = 'setting'
    }
  }


}
