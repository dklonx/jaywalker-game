import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';


export let bussola:any = 'C'

let canvas: any
let ctx: any

// joystick variable
// let this.pressed:any
let circumference = 2 * Math.PI
let internalRadius: any
let maxMoveStick: number
let externalRadius:any
let centerX: number
let centerY: number
let directionHorizontalLimitPos: any;
let directionHorizontalLimitNeg: any
let directionVerticalLimitPos: any
let directionVerticalLimitNeg: any 

// current position of stick
let movedX: number
let movedY: number

let callback: any


@Component({
  selector: 'app-joystick',
  templateUrl: './joystick.component.html',
  styleUrls: ['./joystick.component.css']
})
  
export class JoystickComponent implements OnInit, AfterViewInit {
  @ViewChild('joystick', { static: false }) CANVAS!: ElementRef<HTMLCanvasElement>; 
  pressed: any = 0

  debugTest: any = 'brother'

  private CTX!: CanvasRenderingContext2D | null

  StickStatus:any = {
      xPosition: 0,
      yPosition: 0,
      x: 0,
      y: 0,
      cardinalDirection: "C"
  };

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    // console.log('ngaftervewinit',this)
    this.CTX = this.CANVAS.nativeElement.getContext('2d')
    canvas = this.CANVAS
    ctx = this.CTX

    // initializing variables here
    internalRadius = (canvas.nativeElement.width-((canvas.nativeElement.width/2)+10))/2;
    maxMoveStick = internalRadius + 5
    externalRadius = internalRadius + 30
    centerX = canvas.nativeElement.width / 2
    centerY = canvas.nativeElement.height / 2
    directionHorizontalLimitPos = canvas.nativeElement.width / 30
    directionHorizontalLimitNeg = directionHorizontalLimitPos * -1
    directionVerticalLimitPos = canvas.nativeElement.height / 10
    directionVerticalLimitNeg = directionVerticalLimitPos * -1

    // used to save current positino of stick
    movedX = centerX
    movedY = centerY

    // check if the device support the touch or not
    if("ontouchstart" in document.documentElement)
    {
        window.addEventListener("touchstart", this.onTouchStart.bind(this), false);
        window.addEventListener("touchmove", this.onTouchMove.bind(this), false);
        window.addEventListener("touchend", this.onTouchEnd.bind(this), false);
    }
    else
    {
        document.addEventListener("mousedown", this.onMouseDown.bind(this), false);
        canvas.nativeElement.addEventListener("mousemove", this.onMouseMove.bind(this), false);
        document.addEventListener("mouseup", this.onMouseUp.bind(this), false);
    }

    // draw the Object
    this.drawExternal()
    this.drawInternal()
  }

  // private methods
  drawExternal() { 
    ctx.beginPath()
    ctx.arc(centerX, centerY, externalRadius, 0, circumference, false);
    ctx.lineWidth = 1;
    ctx.strokeStyle = 'blue';
    ctx.stroke();
    
  }

  drawInternal(){   
        ctx.beginPath();
        if(movedX<internalRadius) { movedX=maxMoveStick; }
        if((movedX+internalRadius) > canvas.nativeElement.width) { movedX = canvas.nativeElement.width-(maxMoveStick); }
        if(movedY<internalRadius) { movedY=maxMoveStick; }
        if((movedY+internalRadius) > canvas.nativeElement.height) { movedY = canvas.nativeElement.height-(maxMoveStick); }
        ctx.arc(movedX, movedY, internalRadius, 0, circumference, false);
        // create radial gradient
        var grd = ctx.createRadialGradient(centerX, centerY, 5, centerX, centerY, 200);
        // Light color
        grd.addColorStop(0, 'grey');
        // Dark color
        grd.addColorStop(1, 'black');
        ctx.fillStyle = grd;
        ctx.fill();
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
  }

  onTouchStart(event: any) {
    this.debugTest = 'ontouchstart'
    // ctx.font = "10px Arial";
    // ctx.fillText(event.targetTouches[0].pageX, 10, 50); 
    this.pressed = 1;
    // canvas.nativeElement.style.background = 'green'
    this.drawExternal();
            this.drawInternal();
    
  }

  onTouchMove(event: any) {
      this.debugTest = `pageX ${event.targetTouches[0].pageX} pageY ${event.targetTouches[0].pageY} \n ${this}`
      
      // i removed 
      // && event.targetTouches[0].target === canvas
      // cuz i dont what it does
    if(this.pressed === 1)
    {
        
        movedX = event.targetTouches[0].pageX;
        movedY = event.targetTouches[0].pageY;
        // // Manage offset
        if(canvas.nativeElement.offsetParent.tagName.toUpperCase() === "BODY")
        {
            movedX -= canvas.nativeElement.offsetLeft;
            movedY -= canvas.nativeElement.offsetTop;
        }
        else
        {
            movedX -= canvas.nativeElement.offsetParent.offsetLeft;
            movedY -= canvas.nativeElement.offsetParent.offsetTop;
        }
        // Delete canvas
        ctx.clearRect(0, 0, canvas.nativeElement.width, canvas.nativeElement.height);
        // Redraw object
        this.drawExternal();
        this.drawInternal();

        // Set attribute of callback
        // this.StickStatus.xPosition = movedX;
        // this.StickStatus.yPosition = movedY;
        // this.StickStatus.x = Number((100*((movedX - centerX)/maxMoveStick)).toFixed());
        // this.StickStatus.y = Number(((100*((movedY - centerY)/maxMoveStick))*-1).toFixed());
        // this.StickStatus.cardinalDirection = this.getCardinalDirection();
        bussola = JoystickComponent.getCardinalDirection()

        // i dont know what does callback do here
        // callback(this.StickStatus);
    }
  } 

  onTouchEnd(event: any) {
    this.debugTest = 'ontouchend'
    // canvas.nativeElement.style.background = 'brown'
    this.pressed = 0;
    // If required reset position store variable
    if(true)
    {
        movedX = centerX;
        movedY = centerY;
    }
    // Delete canvas
    ctx.clearRect(0, 0, canvas.nativeElement.width, canvas.nativeElement.height);
    // Redraw object
    this.drawExternal();
    this.drawInternal();

    // Set attribute of callback
    // this.StickStatus.xPosition = movedX;
    // this.StickStatus.yPosition = movedY;
    // this.StickStatus.x = Number((100*((movedX - centerX)/maxMoveStick)).toFixed());
    // this.StickStatus.y = Number(((100*((movedY - centerY)/maxMoveStick))*-1).toFixed());
    // this.StickStatus.cardinalDirection = this.getCardinalDirection();
        bussola = JoystickComponent.getCardinalDirection()
    
    // again i dont know
    // callback(StickStatus);
  }

  onMouseDown(event: any) 
  
  {
    // console.log(this.pressed)
    this.pressed = 1;
    // console.log('onmousedown ')
    
    this.drawExternal();
    this.drawInternal();
  }
  
  onMouseMove(event:any) 
  {
    // console.log('onmoousemove joyu', this.pressed)
    // this.pressed= 1
    // ctx.clearRect(0, 0, canvas.nativeElement.width, canvas.nativeElement.height);

      this.drawExternal();
            this.drawInternal();
    // console.log('onmousemove')
        if(this.pressed === 1)
        {
          // console.log('mouse move pressed')
            movedX = event.pageX;
            movedY = event.pageY;
            // Manage offset
            if(canvas.nativeElement.offsetParent.tagName.toUpperCase() === "BODY")
            {
                movedX -= canvas.nativeElement.offsetLeft;
                movedY -= canvas.nativeElement.offsetTop;
            }
            else
            {
                movedX -= canvas.nativeElement.offsetParent.offsetLeft;
                movedY -= canvas.nativeElement.offsetParent.offsetTop;
            }
            // Delete canvas.nativeElement
            ctx.clearRect(0, 0, canvas.nativeElement.width, canvas.nativeElement.height);
            // Redraw object
            this.drawExternal();
            this.drawInternal();

            // Set attribute of callback
            // this.StickStatus.xPosition = movedX;
            // this.StickStatus.yPosition = movedY;
            // this.StickStatus.x = Number((100*((movedX - centerX)/maxMoveStick)).toFixed());
            // this.StickStatus.y = Number(((100*((movedY - centerY)/maxMoveStick))*-1).toFixed());
            // this.StickStatus.cardinalDirection = this.getCardinalDirection();
        bussola = JoystickComponent.getCardinalDirection()

            // again and again i dont know this 
            // callback(this.StickStatus);
        }
  }
  
  onMouseUp(event:any) 
  {
    // console.log('mouseup')
    this.pressed = 0;

    // If required reset position store variable
    if(this.pressed === 0)
    {
      // console.log('mouseup', this.pressed)
            movedX = centerX;
            movedY = centerY;
        }
        // Delete canvas
        ctx.clearRect(0, 0, canvas.nativeElement.width, canvas.nativeElement.height);
        // Redraw object
        this.drawExternal();
        this.drawInternal();

        // Set attribute of callback
        // this.StickStatus.xPosition = movedX;
        // this.StickStatus.yPosition = movedY;
        // this.StickStatus.x = Number((100*((movedX - centerX)/maxMoveStick)).toFixed());
        // this.StickStatus.y = Number(((100*((movedY - centerY)/maxMoveStick))*-1).toFixed());
        // this.StickStatus.cardinalDirection = this.getCardinalDirection();
        bussola = JoystickComponent.getCardinalDirection()

        // callback(this.StickStatus);
    }


  static getCardinalDirection(){
      let result = "";
      let orizontal = movedX - centerX;
      let vertical = movedY - centerY;

      if(vertical >= directionVerticalLimitNeg && vertical <= directionVerticalLimitPos)
      {
        result = "C";
      }
      if(vertical < directionVerticalLimitNeg)
      {
        result = "N";
      }
      if(vertical > directionVerticalLimitPos)
      {
        result = "S";
      }

      if(orizontal < directionHorizontalLimitNeg)
      {
        if(result === "C")
        { 
            result = "W";
        }
        else
        {
            result += "W";
        }
      }
      if(orizontal > directionHorizontalLimitPos)
      {
        if(result === "C")
        { 
            result = "E";
        }
        else
        {
            result += "E";
        }
      }

      return result;
  }


  // public methods

  getWidth() { 
    return canvas.nativeElement.width
  }

  getHeight() { 
    return canvas.nativeElement.height
  }

  getPosX() { 
    return movedX
  }

  getPosY() {
    return movedY
   }

  GetX = function ()
    {
        return (100*((movedX - centerX)/maxMoveStick)).toFixed();
    };

 
    GetY = function ()
    {
        return ((100*((movedY - centerY)/maxMoveStick))*-1).toFixed();
    };


    // GetDir = function()
    // {
    //     return getCardinalDirection();
    // };
}
