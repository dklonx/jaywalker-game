import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';


export let gamePad:any = ''

let canvas: HTMLCanvasElement
let ctx: CanvasRenderingContext2D


@Component({
  selector: 'app-gamepad',
  templateUrl: './gamepad.component.html',
  styleUrls: ['./gamepad.component.css']
})
export class GamepadComponent implements OnInit, AfterViewInit {
  @ViewChild('gamepad', { static: false }) CANVAS: ElementRef<HTMLCanvasElement> = {} as ElementRef; 


  // private CTX: CanvasRenderingContext2D = {} as CanvasRenderingContext2D

  constructor() { }

  ngOnInit(): void {

  }

  ngAfterViewInit() { 
    canvas = this.CANVAS.nativeElement
    ctx = canvas.getContext('2d') as CanvasRenderingContext2D

    this.controlInit()
    
    this.animate()
  }

  controlInit() { 
    canvas.addEventListener('mousedown', (e) => { 
      const mousePosition = getMousePosition(canvas, e)

      if (mousePosition.x > canvas.width / 2 - 25 &&
        mousePosition.x < canvas.width / 2 - 25 + 50 && 
        mousePosition.y > 0 && 
        mousePosition.y < 50
      ) { 
        // console.log('arrow up')
        gamePad = 'arrowUp'
      }

      if (mousePosition.x > canvas.width/2 - 25 &&
        mousePosition.x < canvas.width/2 - 25 + 50 &&
        mousePosition.y > canvas.height - 50 && 
        mousePosition.y < canvas.height - 50 + 50
      ) { 
        // console.log('arrow down')
        gamePad = 'arrowDown'

      }

      if (mousePosition.x > canvas.width / 2 - 80 &&
        mousePosition.x < canvas.width / 2 - 80 + 50 && 
        mousePosition.y > canvas.height / 2 - 25 &&
        mousePosition.y < canvas.height /2 - 25 + 50
      ) { 
        // console.log('arrow left')
        gamePad = 'arrowLeft'

      }

      if (mousePosition.x > canvas.width / 2 + 30  &&
        mousePosition.x < canvas.width / 2 + 30 + 50 && 
        mousePosition.y > canvas.height / 2 - 25 &&
        mousePosition.y < canvas.height / 2 - 25 + 50
      ) { 
        // console.log('arrow right')
        gamePad = 'arrowRight'

      }

    })

    canvas.addEventListener('mouseup', () => { 
      gamePad = ''
      // console.log('mouseup')
    })

    canvas.addEventListener('touchend', () => { 
      gamePad = ''
      // console.log('mouseup')
    })

    canvas.addEventListener('touchstart', (e) => { 
      const mousePosition = getInteractionLocation(e)

      if (mousePosition.x > canvas.width / 2 - 25 &&
        mousePosition.x < canvas.width / 2 - 25 + 50 && 
        mousePosition.y > 0 && 
        mousePosition.y < 50
      ) { 
        // console.log('arrow up')
        gamePad = 'arrowUp'
      }

      if (mousePosition.x > canvas.width/2 - 25 &&
        mousePosition.x < canvas.width/2 - 25 + 50 &&
        mousePosition.y > canvas.height - 50 && 
        mousePosition.y < canvas.height - 50 + 50
      ) { 
        // console.log('arrow down')
        gamePad = 'arrowDown'

      }

      if (mousePosition.x > canvas.width / 2 - 80 &&
        mousePosition.x < canvas.width / 2 - 80 + 50 && 
        mousePosition.y > canvas.height / 2 - 25 &&
        mousePosition.y < canvas.height /2 - 25 + 50
      ) { 
        // console.log('arrow left')
        gamePad = 'arrowLeft'

      }

      if (mousePosition.x > canvas.width / 2 + 30  &&
        mousePosition.x < canvas.width / 2 + 30 + 50 && 
        mousePosition.y > canvas.height / 2 - 25 &&
        mousePosition.y < canvas.height / 2 - 25 + 50
      ) { 
        // console.log('arrow right')
        gamePad = 'arrowRight'

      }

    })


  }

  drawAll() { 
    // arrow up
    ctx.beginPath()
    ctx.fillStyle = 'rgba(138, 80, 110, 0.8)'
    ctx.fillRect(canvas.width/2 - 25, 0, 50, 50)
    ctx.closePath()

    // arrow down
    ctx.beginPath()
    ctx.fillStyle = 'rgba(138, 80, 110, 0.8)'
    ctx.fillRect(canvas.width/2 - 25, canvas.height - 50, 50, 50)
    ctx.closePath()

    // arrow left
    ctx.beginPath()
    ctx.fillStyle = 'rgba(138, 80, 110, 0.8)'
    ctx.fillRect(canvas.width/2 -80 + 5, canvas.height /2 - 25,50, 50)
    ctx.closePath()

    // arrow right
    ctx.beginPath()
    ctx.fillStyle = 'rgba(138, 80, 110, 0.8)'
    ctx.fillRect(canvas.width / 2 + 25 , canvas.height / 2 - 25,50, 50)
    ctx.closePath()

    ctx.beginPath()
    ctx.moveTo(canvas.width/2 - 25, 0)
    ctx.lineTo(canvas.width/2 - 25, 50)
    ctx.lineTo(canvas.width/2 - 25 - 50, 50)
    ctx.lineTo(canvas.width/2 - 25 - 50, 100)
    ctx.lineTo(canvas.width/2 - 25 , 100)
    ctx.lineTo(canvas.width/2 - 25 , 150)
    ctx.lineTo(canvas.width/2 - 25 , 150)
    ctx.lineTo(canvas.width/2 - 25 + 50 , 150)
    ctx.lineTo(canvas.width/2 - 25 + 50 , 100)
    ctx.lineTo(canvas.width/2 - 25 + 100 , 100)
    ctx.lineTo(canvas.width/2 - 25 + 100 , 50)
    ctx.lineTo(canvas.width/2 - 25 + 50 , 50)
    ctx.lineTo(canvas.width/2 - 25 + 50 , 0)
    ctx.closePath()

    ctx.strokeStyle = 'black'
    ctx.lineWidth = 2
    ctx.stroke()
    ctx.fillStyle = '#696a6f'
    ctx.fill()

    ctx.beginPath()
    ctx.arc(canvas.width/2 - 25 + 25, 75, 15, 0, 2 * Math.PI)
    ctx.stroke()
    ctx.fillStyle = '#56575c'
    ctx.fill()
    ctx.strokeStyle = 'black'
    ctx.stroke()

    // ctx.rect(135, 10, 30, 30)
    // ctx.stroke()
    ctx.beginPath()
    ctx.moveTo(135 + 15, 10)
    ctx.lineTo(135, 40)
    ctx.lineTo(165, 40)
    ctx.closePath()
    ctx.fillStyle = '#56575c'
    ctx.fill()
    ctx.strokeStyle = 'black'
    ctx.stroke()


    // ctx.rect(85, 60, 30, 30)
    // ctx.stroke()
    ctx.beginPath()
    ctx.moveTo(85, 60 +15)
    ctx.lineTo(85 + 30, 60)
    ctx.lineTo(85 + 30, 90)
    ctx.closePath()
    ctx.fillStyle = '#56575c'
    ctx.fill()
    ctx.strokeStyle = 'black'
    ctx.stroke()
    

    // ctx.rect(85 * 2 + 15, 60, 30, 30)
    // ctx.stroke()
    ctx.beginPath()
    ctx.moveTo(85 * 2 + 15, 60)
    ctx.lineTo(85 * 2 + 15 + 30, 60 + 15)
    ctx.lineTo(85 * 2 + 15, 60 + 30)
    ctx.closePath()
    ctx.fillStyle = '#56575c'
    ctx.fill()
    ctx.strokeStyle = 'black'
    ctx.stroke()

    // ctx.rect(135, 110, 30, 30)
    // ctx.stroke()

    ctx.beginPath()
    ctx.moveTo(135 + 15, 140)
    ctx.lineTo(135, 110)
    ctx.lineTo(165,110)
    ctx.closePath()
    ctx.fillStyle = '#56575c'
    ctx.fill()
    ctx.strokeStyle = 'black'
    ctx.stroke()
  }


  animate() { 
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    this.drawAll()

    requestAnimationFrame(this.animate.bind(this))
  }

}

function getMousePosition(canvas:any, event:any) {
        let mouseX = event.offsetX
        let mouseY = event.offsetY

        let x = mouseX * canvas.width / canvas.clientWidth
        let y = mouseY * canvas.height / canvas.clientHeight

        return {x, y}
    }


    function getInteractionLocation(event:any) {
      let pos = { x: event.clientX, y: event.clientY };
      if (event.touches) {
          pos = { x: event.touches[0].clientX, y: event.touches[0].clientY };
      }
      const rect = event.target.getBoundingClientRect();
      const x_rel = pos.x - rect.left;
      const y_rel = pos.y - rect.top;
      const x = Math.round((x_rel * event.target.width) / rect.width);
      const y = Math.round((y_rel * event.target.height) / rect.height);
      return {x, y};
  };