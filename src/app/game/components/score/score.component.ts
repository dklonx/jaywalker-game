import { Component, OnInit } from '@angular/core';

export const scoreSystem = {
  score: 0
}

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css']
})
export class ScoreComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  getScore(){
    return scoreSystem.score
  }

}
