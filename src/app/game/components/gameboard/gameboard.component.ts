import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Input } from '@angular/core';
import { addObject, addTextObjects, animate, canvas, ctx, ctxText, getMousePosition, initCanvas, initTextCanvas, setCanvas, setTextCanvas, textCanvas } from '../../ng-canvas/canvas';
import { Bike } from '../../ng-canvas/classes/Bike';
import { Car } from '../../ng-canvas/classes/Car';
import { Fiume } from '../../ng-canvas/classes/Fiume';
import { Square } from '../../ng-canvas/classes/Square';
import { initControls} from '../../ng-canvas/events';
import { setMainCharacter, setObstacles } from '../../ng-canvas/variables';
import { JoystickComponent } from '../joystick/joystick.component';
// import { checkCollisionCanvas } from '../../ng-canvas/utilities';
import { gamePad } from '../gamepad/gamepad.component';

import { SettingsScreenComponent, universalSettings } from '../settings-screen/settings-screen.component';

@Component({
  selector: 'app-gameboard',
  templateUrl: './gameboard.component.html',
  styleUrls: ['./gameboard.component.css']
})
export class GameboardComponent implements OnInit, AfterViewInit {
  @ViewChild('canvas1', { static: false }) CANVAS: ElementRef<HTMLCanvasElement> = {} as ElementRef; 
  @ViewChild(JoystickComponent) child!: JoystickComponent
  // @ViewChild(SettingsScreenComponent) settings!: SettingsScreenComponent

  // @ViewChild('textCanvas', { static: false }) TEXTCANVAS: ElementRef<HTMLCanvasElement> = {} as ElementRef; 

  // gamePad:any
   controllerType:string = 'joystick'

  constructor() { 
    
  }

setSetting(event:any){ 
  console.log('setSetting', event)
  // this.universalSettings = event
  this.controllerType = event
}

  ngOnInit(): void {

  }

  ngAfterViewInit() { 

    initControls()
    
    // console.log(this.child.StickStatus.cardinalDirection)
    initCanvas(this.CANVAS.nativeElement)
    // initTextCanvas(this.TEXTCANVAS.nativeElement)

    if (canvas !== null) setCanvas(350,350 , 'solid 2px red')
    // if (setTextCanvas !== null) setTextCanvas(350,350)

    // checkCollisionCanvas(square, square)
    const fiume = new Fiume(canvas.width-200, 50,0,130)
    addObject(fiume)

    const fiume2 = new Fiume(90, 50, canvas.width - 90, 130)
    addObject(fiume2)

     const car = new Car()
    addObject(car)

    const bike = new Bike()
    addObject(bike)

    setObstacles(car, bike)
    setObstacles(fiume, fiume2)
    
    const square = new Square(45, 45, 'blue')
    addObject(square)
    setMainCharacter(square)


    // const button = {
    //     width: 100,
    //     height: 50,
    //     x: textCanvas.width / 2 - 140,
    //   y: textCanvas.height / 2,
    //   draw: function() { 

    //   },
    //   update: function () { 

    //   }

    // }

    // const modal = {
    //     width: 300,
    //     height: 200,
    //     x: -310,
    //     y: textCanvas.height / 2 - 100,
    //     open: true,
    //     draw: function (){
    //         ctxText.beginPath()
    //         ctxText.strokeStyle = 'yellow'
    //         ctxText.lineWidth = 2
    //         ctxText.rect(this.x, this.y, this.width, this.height)
    //         ctxText.stroke()
    //         ctxText.closePath()

    //         ctxText.font = "30px Arial";
    //         ctxText.fillStyle = 'yellow'
    //         ctxText.fillText("clicca e chiudi", this.x + 20, this.y + 40); 

    //         ctxText.fillStyle = 'purple'
    //         ctxText.fillRect(this.x + 50, this.y + 100, button.width, button.height)
    //     },
    //     update: function () {
    //         // if (this.open) {
    //             if (this.x < textCanvas.width / 2 - 150) {
    //                 this.x += 10
    //             }
    //         // } else { 
    //         //     if (this.x > -310) { 
    //         //         this.x -= 10
    //         //     }
    //         // }
    //     },
    // }
    
//     textCanvas.addEventListener('click', (e) => { 
//     const MousePosition = getMousePosition(canvas, e)
//     if (MousePosition.x >=  &&
//         MousePosition.x <= button.x + button.width && MousePosition.y >= button.y && 
//         MousePosition.y <= button.y + button.height
//     ) { 
//       console.log('cliccato')
//         modal.open = false
//         canvas.style.zIndex = '1'
//         textCanvas.style.zIndex = '-1'
//     }
// })

    // addTextObjects(button)
    // addTextObjects(modal)

    animate()
  }



}
