import { canvas } from "./canvas"
import { Fiume } from "./classes/Fiume"

export let currentKey: string = ''

export const ghost = new Image()
ghost.src = '/assets/ghost.png'

export const carSpritesheet = new Image()
carSpritesheet.src = '/assets/cars.png'

export const bikeSpritesheet = new Image()
bikeSpritesheet.src = '/assets/bike.png'

export function setCurrentKey(key: string) { 
    currentKey = key
}

export let obstacles: any = []
export let mainCharacter:any

export function setObstacles(c: any, d:any) { 
    obstacles.push(c)
    obstacles.push(d)

}

export function setMainCharacter(a:any) { 
    mainCharacter = a
}

