import { transition } from "@angular/animations"
import { Square } from "./classes/Square"
import { handleObstacles, traguardo } from "./utilities"

export let canvas: HTMLCanvasElement
export let ctx: CanvasRenderingContext2D

export let textCanvas: HTMLCanvasElement
export let ctxText: CanvasRenderingContext2D

let objects2Animate: Square[] | any = []

let textCanvasObjects: any = []

export function initCanvas(CANVAS:HTMLCanvasElement) {
    canvas = CANVAS
    // initCtx(canvas)
    ctx = canvas.getContext('2d') as CanvasRenderingContext2D
}

export function initTextCanvas(TEXTCANVAS: HTMLCanvasElement) { 
    textCanvas = TEXTCANVAS
    // ctxText = textCanvas.getContext('2d') as CanvasRenderingContext2D
}

export function setCanvas(width: number, height: number, color:string = ''){ 
    canvas.width = width
    canvas.height = height
    canvas.style.border = color 
}

export function setTextCanvas(width: number, height: number) { 
    textCanvas.width = width
    textCanvas.height = height
    textCanvas.style.background = 'rgba(0, 0, 0, 0.5)'
}

export function addObject(obj:Square|any) { 
    objects2Animate.push(obj)
    // console.log(objects2Animate)
}


const ponte = {
    width: 110,
    height: 70,
    x: 150,
    y: 120,
    draw() {
        ctx.fillStyle = 'grey'
        ctx.fillRect(this.x, this.y, this.width, this.height)
    },
    update(){
        // console.log('update')
    }
}

export function addTextObjects(obj:any) { 
    textCanvasObjects.push(obj)
}


function drawAll() { 

    //marciapiede
    ctx.fillStyle = 'grey'
    ctx.fillRect(0, canvas.height - 70 ,canvas.width, 70)

    // strada
    ctx.fillStyle = 'black'
    ctx.fillRect(0, canvas.height - 70 - 100, canvas.width, 100) 
    
    // strisce
    ctx.fillStyle = 'white'
    for (let i = 0; i < 4; i++) { 
        ctx.fillRect(180, canvas.height - 100 - 20 * i , 50, 10)
    }

    // pista ciclabile terra
    ctx.fillStyle = 'brown'
    ctx.fillRect(0, 70, canvas.width, 60)

    // ponte
    // ponte.update()
    // ponte.draw()

    // prato
    ctx.fillStyle = 'green'
    ctx.fillRect(0, 0, canvas.width,70)

    objects2Animate.forEach((obj: Square) => { 
        // console.log(obj)
        obj.draw()
    })

    textCanvasObjects.forEach((obj: any) => {
        obj.draw()
    })
}


function updateAll() { 
    objects2Animate.forEach((obj: Square) => { 
        obj.update()
    })

    textCanvasObjects.forEach((obj: any) => {
        obj.update()
    })

}

export function animate() { 
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    // ctxText.clearRect(0,0, textCanvas.width, textCanvas.height)
    drawAll()
    updateAll()
    handleObstacles()
    traguardo()



    
    requestAnimationFrame(animate)


}

export function getMousePosition(canvas:any , event:any) {
    let mouseX = event.offsetX
    let mouseY = event.offsetY

    let x = mouseX * canvas.width / canvas.clientWidth
    let y = mouseY * canvas.height / canvas.clientHeight

    return {x, y}
}

