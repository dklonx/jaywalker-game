import { canvas, textCanvas } from "./canvas"
import { setCurrentKey } from "./variables"

export function initControls() { 
    window.addEventListener('keydown', (e: KeyboardEvent) => { 
        if (e.key.includes('a') || e.key.includes('d') || e.key.includes('s') || e.key.includes('w')) {
            setCurrentKey(e.key)
        } else { 
            // console.log('event key non riconosciuto')
        }
        
    })

    window.addEventListener('keyup', () => { 
        setCurrentKey('')
    })
    
   
}




function getMousePosition(canvas:any, event:any) {
    let mouseX = event.offsetX
    let mouseY = event.offsetY

    let x = mouseX * canvas.width / canvas.clientWidth
    let y = mouseY * canvas.height / canvas.clientHeight

    return {x, y}
}




