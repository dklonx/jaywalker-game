import { bussola } from "../../components/joystick/joystick.component"
import { canvas, ctx, textCanvas } from "../canvas"
import { insideCanvas, resetGame } from "../utilities"
import { currentKey, ghost } from "../variables"

import { gamePad } from "../../components/gamepad/gamepad.component"
import { scoreSystem } from "../../components/score/score.component"

export class Square{ 
    width: number
    height: number
    color: string
    coordinates: { x: number, y: number }
    speed: number
    direction: string
    constructor(width:number, height: number, color: string) { 
        this.width = width - 10
        this.height = height -5
        this.color = color
        this.coordinates = { x: 0, y: canvas.height - this.height - 10 }
        this.speed = 1.3
        this.direction = 'right'
    }

    update() { 
        
        // if (insideCanvas(this, canvas)) {
        //     // console.log('dentro il canvas')
        //     // console.log(currentKey)
        //     switch (currentKey) {
        //         case 'a': {
        //             this.coordinates.x -= this.speed
        //             // console.log('sinistra')
        //             break
        //         }
                    
        //         case 'd': {
        //             this.coordinates.x += this.speed
        //             // console.log('destra')
        //             break
        //         }
                    
        //         case 'w': { 
        //             this.coordinates.y -= this.speed
        //             break
        //         }
                    
        //         case 's': { 
        //             this.coordinates.y += this.speed
        //             break
        //         }
                    
        //         default: {

        //         }
        //     }
        // }
        // else { 
        //     // console.log(currentKey)
        //     if (this.coordinates.x <= 0 && currentKey == 'd') {
        //         this.coordinates.x += this.speed
        //     } else if (this.coordinates.x + this.width >= canvas.width && currentKey == 'a') {
        //         this.coordinates.x -= this.speed
        //     } else if (this.coordinates.y <= 0 && currentKey == 's') {
        //         this.coordinates.y += this.speed
        //     } else if (this.coordinates.y + this.height>= canvas.height && currentKey == 'w') { 
        //         this.coordinates.y -= this.speed
        //     }


            
        // }


        // // comandi joystick
        // if (insideCanvas(this, canvas)) {
        //     // console.log('dentro il canvas')
        //     // console.log(currentKey)
        //     switch (bussola) {
        //         case 'W': {
        //             this.coordinates.x -= this.speed
        //             // console.log('sinistra')
        //             break
        //         }
                    
        //         case 'E': {
        //             this.coordinates.x += this.speed
        //             // console.log('destra')
        //             break
        //         }
                    
        //         case 'N': { 
        //             this.coordinates.y -= this.speed
        //             break
        //         }
                    
        //         case 'S': { 
        //             this.coordinates.y += this.speed
        //             break
        //         }
        //         case 'NE': { 
        //             this.coordinates.x += this.speed
        //             this.coordinates.y -= this.speed
        //             break
        //         }
        //         case 'NW': { 
        //             this.coordinates.x -= this.speed
        //             this.coordinates.y -= this.speed
        //             break
        //         }   
                    
        //         case 'SE': { 
        //             this.coordinates.x += this.speed
        //             this.coordinates.y += this.speed
        //             break
        //         }
        //         case 'SW': { 
        //             this.coordinates.x -= this.speed
        //             this.coordinates.y += this.speed
        //             break
        //         }
                    
        //         default: {

        //         }
        //     }
        // }
        // else { 
        //     // console.log(currentKey)
        //     if (this.coordinates.x <= 0 && bussola == 'E') {
        //         this.coordinates.x += this.speed
        //     } else if (this.coordinates.x + this.width >= canvas.width && bussola == 'W') {
        //         this.coordinates.x -= this.speed
        //     } else if (this.coordinates.y <= 0 && bussola == 'S') {
        //         this.coordinates.y += this.speed
        //     } else if (this.coordinates.y + this.height >= canvas.height && bussola == 'N') {
        //         this.coordinates.y -= this.speed
        //     } else if (this.coordinates.x + this.width >= canvas.width && bussola == 'NW') { 
        //         this.coordinates.y -= this.speed
        //         this.coordinates.x += this.speed

        //     }else if (this.coordinates.x + this.width >= canvas.width && bussola == 'SW') { 
        //         this.coordinates.y += this.speed
        //         this.coordinates.x -= this.speed

        //     } else if (this.coordinates.y <= 0 && bussola == 'NW') {
        //         this.coordinates.y -= this.speed
        //         this.coordinates.x -= this.speed

        //     } else if (this.coordinates.y <= 0 && bussola == 'NE') {
        //         this.coordinates.y -= this.speed
        //         this.coordinates.x += this.speed

        //     }

        // }

        // // death zone === fiume1
        // if (this.coordinates.x >= 0 && this.coordinates.x + this.width < canvas.width - 180 &&
        //     this.coordinates.y + this.width >= 130 && this.coordinates.y <= 160
        // ) { 
        //     // textCanvas.style.zIndex = '1'
        //     // canvas.style.zIndex = '-1'
        //     resetGame()
        //     scoreSystem.score -= 100
        // }

        // // death zone fiume2
        // if (this.coordinates.x + 5 >= canvas.width - 90 && this.coordinates.x + this.width <= canvas.width &&
        //     this.coordinates.y + this.height > 135 && this.coordinates.y <= 150 
        // ) { 
        //     // textCanvas.style.zIndex = '1'
        //     // canvas.style.zIndex = '-1'
        //         resetGame()
        //     scoreSystem.score -= 100

        // }
        
        // if (insideCanvas(this, canvas)) {
        //     switch (gamePad) {
        //         case 'arrowUp': {
        //             this.coordinates.y -= this.speed
        //             break
        //         }
        //         case 'arrowDown': {
        //             this.coordinates.y += this.speed
                    
        //             break
        //         }
        //         case 'arrowLeft': {
        //             this.coordinates.x -= this.speed
                    
        //             break
        //         }
        //         case 'arrowRight': {
        //             this.coordinates.x += this.speed

        //             break
        //         }
                    
        //         default: {

        //         }
        //     }
        // } else { 
        //     if (this.coordinates.x <= 0 && gamePad == 'arrowRight') {
        //         this.coordinates.x += this.speed
        //     } else if (this.coordinates.x + this.width >= canvas.width && gamePad == 'arrowLeft') {
        //         this.coordinates.x -= this.speed
        //     } else if (this.coordinates.y <= 0 && gamePad == 'arrowDown') {
        //         this.coordinates.y += this.speed
        //     } else if (this.coordinates.y + this.height >= canvas.height && gamePad == 'arrowUp') {
        //         this.coordinates.y -= this.speed
        //     }
        // }



        switch(currentKey){

            case 'w':{
                if(this.coordinates.y >= 0){
                    this.coordinates.y -= this.speed
                }

                break
            }

            case 'a':{
                if(this.coordinates.x >= 0){
                    this.coordinates.x -= this.speed
                }

                break
            }

            case 'd':{
                if(this.coordinates.x + this.width  <= canvas.width){
                    this.coordinates.x += this.speed
                }

                break
            }

            case 's':{
                if(this.coordinates.y + this.height <= canvas.height){
                    this.coordinates.y += this.speed
                }

                break
            }



            default:{
                // console.log('bruh your are not moving')
            }
        }

        switch(bussola){

            case 'W': {
                // this.coordinates.x -= this.speed
                


                if(this.coordinates.x >= 0){
                    this.coordinates.x -= this.speed
                }
               
                // console.log('sinistra')
                break
            }
                
            case 'E': {
                // this.coordinates.x += this.speed
                if(this.coordinates.x + this.width  <= canvas.width){
                    this.coordinates.x += this.speed
                }
                // console.log('destra')
                break
            }
                
            case 'N': { 
                // this.coordinates.y -= this.speed
                if(this.coordinates.y >= 0){
                    this.coordinates.y -= this.speed
                }

                break
            }
                
            case 'S': { 
                // this.coordinates.y += this.speed

                if(this.coordinates.y + this.height <= canvas.height){
                    this.coordinates.y += this.speed
                }
                break
            }
            case 'NE': { 
                // this.coordinates.x += this.speed
                // this.coordinates.y -= this.speed

                if(this.coordinates.y >= 0 && this.coordinates.x + this.width  <= canvas.width){
                    this.coordinates.y -= this.speed
                    this.coordinates.x += this.speed

                }
                break
            }
            case 'NW': { 
                // this.coordinates.x -= this.speed
                // this.coordinates.y -= this.speed

                if(this.coordinates.y >= 0 && this.coordinates.x >= 0){
                    this.coordinates.y -= this.speed
                    this.coordinates.x -= this.speed

                }
                break
            }   
                
            case 'SE': { 
                // this.coordinates.x += this.speed
                // this.coordinates.y += this.speed

                if(this.coordinates.y + this.height <= canvas.height && this.coordinates.x + this.width  <= canvas.width){
                    this.coordinates.y += this.speed
                    this.coordinates.x += this.speed

                }


                break
            }
            case 'SW': { 
                // this.coordinates.x -= this.speed
                // this.coordinates.y += this.speed

                if(this.coordinates.y + this.height <= canvas.height && this.coordinates.x >= 0){
                    this.coordinates.y += this.speed
                    this.coordinates.x -= this.speed

                }

                break
            }
                
            default: {

            }
        }

        switch (gamePad) {
            case 'arrowUp': {
                // this.coordinates.y -= this.speed
                if(this.coordinates.y >= 0){
                    this.coordinates.y -= this.speed
                }
                break
            }
            case 'arrowDown': {
                // this.coordinates.y += this.speed

                if(this.coordinates.y + this.height <= canvas.height){
                    this.coordinates.y += this.speed
                }
                
                break
            }
            case 'arrowLeft': {
                // this.coordinates.x -= this.speed
                if(this.coordinates.x >= 0){
                    this.coordinates.x -= this.speed
                }

                
                break
            }
            case 'arrowRight': {
                // this.coordinates.x += this.speed
                
                if(this.coordinates.x + this.width  <= canvas.width){
                    this.coordinates.x += this.speed
                }

                break
            }
                
            default: {

            }
        }

    }

    draw() { 
        ctx.fillStyle = this.color
        ctx.fillRect(this.coordinates.x, this.coordinates.y, this.width, this.height)
        ctx.drawImage(ghost, 0, 0, ghost.width / 8, ghost.height/9, this.coordinates.x- 5, this.coordinates.y, this.width + 10, this.height)
    }
}