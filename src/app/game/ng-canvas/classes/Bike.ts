import { canvas, ctx } from "../canvas"
import { bikeSpritesheet } from "../variables"

export class Bike { 
    width: number
    height: number
    coordinates: { x: number, y: number }
    speed: number
    constructor() { 
        this.width = 50
        this.height = 40
        this.coordinates = {x: canvas.width - 80, y: 75}
        this.speed =.8
    }

    draw() { 
        // ctx.fillStyle = 'yellow'
        // ctx.fillRect(this.coordinates.x, this.coordinates.y, this.width, this.height)
        ctx.drawImage(bikeSpritesheet, this.coordinates.x, this.coordinates.y, this.width, this.height)
    }

    update() { 
        if (this.coordinates.x + this.width > 0) {
            this.coordinates.x -= this.speed
        } else { 
            this.coordinates.x = canvas.width + this.width
        }
    }
}