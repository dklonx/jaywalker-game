import { canvas, ctx } from "../canvas"
import { carSpritesheet } from "../variables"

export class Car { 
    width: number
    height: number
    coordinates: { x: number, y: number }
    speed: number
    constructor() { 
        this.width = 70
        this.height = 50
        this.coordinates = { x: 10, y: 210 }
        this.speed = 1.2
    }

    draw() { 
        // ctx.fillStyle = 'red'
        // ctx.fillRect(this.coordinates.x, this.coordinates.y, this.width, this.height)
        ctx.drawImage(carSpritesheet, 0,0, carSpritesheet.width/2, carSpritesheet.height/3, this.coordinates.x, this.coordinates.y, this.width, this.height)
        
    }

    update() { 
        if (this.coordinates.x < canvas.width) {
            this.coordinates.x += this.speed
        } else { 
            this.coordinates.x = 0 - this.width
        }
    }
    
}