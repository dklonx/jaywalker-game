import { canvas, ctx } from "../canvas"

export class Fiume { 
    width: number
    height: number
    coordinates: {x:number, y: number}
    constructor(width: number, height:number, x:number, y:number) { 
        this.width = width
        this.height = height
        this.coordinates = { x: x, y: y }
        
    }

    draw() { 
        ctx.fillStyle = 'lightblue'
        ctx.fillRect(this.coordinates.x, this.coordinates.y, this.width, this.height)
    }

    update() { }

}