import { scoreSystem } from "../components/score/score.component";
import { canvas } from "./canvas";
import { Fiume } from "./classes/Fiume";
import { Square } from "./classes/Square";
import { mainCharacter, obstacles } from "./variables";

type canvasEntity = Square | HTMLCanvasElement

export function insideCanvas(obj1:canvasEntity, obj2:canvasEntity): boolean | string { 
    const entity1: canvasEntity = obj1 instanceof Square ? obj1 as Square : obj1 as HTMLCanvasElement 
    const entity2: canvasEntity = obj2 instanceof Square ? obj2 as Square : obj2 as HTMLCanvasElement 
    let result: boolean |string = false
    // console.log(entity1, entity2)
    if(entity1 instanceof Square && entity2 instanceof Square) result = 'errore: due entity square'
    if ((entity1 instanceof Square && entity2 instanceof HTMLCanvasElement)) { 
       if(entity1.coordinates.x >= 0 && entity1.coordinates.x + entity1.width < entity2.width && entity1.coordinates.y >=0 && entity1.coordinates.y + entity1.width < entity2.width) result = true
    }
    return result
}

export function collision(first: any, second: any) { 
    return !(first.coordinates.x > second.coordinates.x + second.width || 
        first.coordinates.x + first.width < second.coordinates.x ||
        first.coordinates.y > second.coordinates.y + second.height || 
        first.coordinates.y + first.height < second.coordinates.y
        )

}

export function handleObstacles() { 
    obstacles.forEach((obj: any) => { 
        
        if (collision(mainCharacter, obj) && !(obj instanceof Fiume)) {
            console.log('scontro avvenuti')
            scoreSystem.score -= 100
            resetGame()
        } else { 
            // console.log('scontro non avvnuto')
            // console.log('istanza di fiume')
            if(mainCharacter.coordinates.x > obj.coordinates.x + obj.width || 
                mainCharacter.coordinates.x + mainCharacter.width < obj.coordinates.x + 10 ||
                mainCharacter.coordinates.y > obj.coordinates.y + obj.height - 20 || 
                mainCharacter.coordinates.y + mainCharacter.height < obj.coordinates.y
            ){
                // console.log('fuori fiume')
            }else{
                // console.log('dentro fiume')
                resetGame()

            }
        }
        // console.log(obj)
        // console.log(mainCharacter.coordinates.y)

        
    })
}

export function traguardo() { 
    if (mainCharacter.coordinates.y < 20) { 
        resetGame()
        scoreSystem.score += 100
    }
}

export function resetGame(){ 
    mainCharacter.coordinates.x = 0
    mainCharacter.coordinates.y = canvas.height - mainCharacter.height - 10
}
