import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameboardComponent } from './components/gameboard/gameboard.component';
import { JoystickComponent } from './components/joystick/joystick.component';
import { GamepadComponent } from './components/gamepad/gamepad.component';
import { SettingsScreenComponent } from './components/settings-screen/settings-screen.component';
import { ScoreComponent } from './components/score/score.component';



@NgModule({
  declarations: [
    GameboardComponent,
    JoystickComponent,
    GamepadComponent,
    SettingsScreenComponent,
    ScoreComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    GameboardComponent
  ]
})
export class GameModule { }
